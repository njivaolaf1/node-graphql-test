# TheFork backend test

## Goal
Creation of a graphql API in NodeJS.

## Context
The purpose of this project is to create a simple graphQL API in charge of exposing Restaurant information fetched from an external service (`image-service`) and from a local database.

### Infrastructure
Infrastructure is available in the [docker-compose file](./docker-compose.yml) and composed as follow:
 - a NodeJs GraphQL micro-service ([restaurant-service](./restaurant-service)) You can use the playground to test it http://localhost:3000/graphql
 - a PostgreSQL database
 - a REST micro-service ([image-service](./image-service))

### Database

Database tables are created through migrations and the database is populated with test data through seeds (you don't need to modify migrations and seed directories)

> - restaurant (restaurant_uuid, name, country_code)
> - restaurant_has_images (restaurant_uuid, image_uuid)
> - country (country_code, locales) - (locales is an psql array of texts)


### Image service (DO NOT EDIT THIS PROJECT)
This service is exposing 2 endpoints:
- `GET /images` returning all images (no filter)

Expected response:
```json
{
    "images": [
        {
            "imageUuid": "imageUuid",
            "url": "url"
        }
    ]
}
```

Important information:
 - **This endpoint must NOT be modified** (even if it's badly designed)
 - This project has no database. Data are persisted in memory only. Every server restart will reset everything.

## TO DO

### Create a graphQL query
Create a graphQL query returning restaurant information.

Response must be paginated (set a default value if none is provided) and optionnaly filtered by:
 - the restaurant name
 - with images only (i.e. return only restaurants having images. By default, all restaurants are returned)

Expected response:
```json
{
  "restaurants": [
      {
          "restaurantUuid": "restaurantUuid",
          "name": "name",
          "country": {
              "code": "code",
              "locales": ["fr_FR"]
          },
          "images": [ "http://image.url" ],
          "allowReview": true
      }
  ],
  "pagination": {
      "total": 1,
      "pageCount": 1,
      "currentPage": 1
  }
}
```

### Additional information
- This project is initialized with a basic config. You must keep this stack but feel free **to improve the project structure** if you see any improvement
- Please produce code with what is for you **production quality standards** as if you'll have to maintain codebase during the next coming years
- To provide discussion material for technical interview, you can list any optimizations that you don't have time to implement but consider important

------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Task Section:

## Installation ( development environment ):

- install Makefile
`make setup`
`make build`

- (optional:) Install portainer for visualizing and managing the containers

## Running the services:
`make docker_up`



## To get restaurant informations:
- we can send the query below (name is case "insensitive"): 
```
query restaurants {
  restaurantsInformations(perPage: 10, currentPage: 1, name: "C", withImagesOnly: true){
    pagination {total, pageCount, currentPage }
    restaurants { 
      restaurantUuid: restaurant_uuid, 
      name, 
      allowReview, 
      country {code, locales}
      images, 
  	}
  }
}
```

## Next-steps:
- add allowReview field to the DB ( and clarify the use of that )

- abstract the pagination part so that we can "attach" it to knex ( to be re-usable by other repositories using SQLDataSource )

- add error handling ( avoid sending too much details when internal server error )

- implement 100% code-coverage on /src files

- implement mutation testing and specify a minimum % passing to ensure the quality of test cases

- add integration tests

- add environment-specific configurations ( for security reasons ) so we do not have to commit production server passwords

- add filters on /images route in the image-service so that we can query images by uuids 