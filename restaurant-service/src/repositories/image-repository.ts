'use strict';

import { HTTPCache, RESTDataSource } from 'apollo-datasource-rest';
import { IImagesResponse } from '../interfaces/image';

interface IConfig {
    baseUrl: string;
}
export class ImageRepository extends RESTDataSource {
    constructor(config: IConfig) {
        super();
        this.baseURL = config.baseUrl;
        this.httpCache = new HTTPCache();
    }

    async getImages() {
        return this.get<IImagesResponse>('images')
    }
}