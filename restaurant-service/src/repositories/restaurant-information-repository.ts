'use strict';

import { SQLDataSource } from 'datasource-sql';
import * as _ from 'lodash';
import { IPagination } from '../interfaces/pagination';
import { IRestaurantHasImagesResponse, IRestaurantInformationResponse } from '../interfaces/restaurant';

interface Query {
    queryBodies: string[];
    acceptedFields: string[];
}

interface RestaurantInformationReq {
    perPage: number;
    currentPage: number;
    name: string;
    withImagesOnly: boolean;
}

export class RestaurantInfoRepository extends SQLDataSource {
    constructor(config: Object) {
        super(config);
    }
    initialize() { }

    addFilterWithImageOnly(query: Query, withImagesOnly: boolean) {
        if (!withImagesOnly) return;
        query.queryBodies.push(`AND Restaurant.restaurant_uuid IN ( SELECT DISTINCT restau_has_img.restaurant_uuid FROM ( SELECT COUNT(image_uuid) as image_count,restaurant_uuid from restaurant_has_image GROUP BY restaurant_uuid ) AS restau_has_img)`);
        return;
    }

    async getRestaurantsInformations(reqData: RestaurantInformationReq) {
        const perPage = reqData.perPage || 10;
        const page = reqData.currentPage || 1;
        const searchName = reqData.name || '';
        const withImagesOnly = reqData.withImagesOnly;

        const offset = (page - 1) * perPage;
        const query: Query = {
            queryBodies: [`FROM Restaurant LEFT JOIN Country ON Restaurant.country_code=Country.country_code WHERE Restaurant.name ILIKE '%' || ? || '%'`],
            acceptedFields: ['restaurant_uuid', 'Restaurant.country_code as country_code', 'name', 'Country.locales as country_locales']
        };

        this.addFilterWithImageOnly(query, withImagesOnly);


        const { queryBodies, acceptedFields } = query
        const queryBodyStr = queryBodies.join(' ');
        const acceptedFieldsStr = acceptedFields.join(',');

        return Promise.all([
            this.knex.raw(`SELECT COUNT(*) AS count ${queryBodyStr}`, [searchName]),
            this.knex.raw(`SELECT ${acceptedFieldsStr} ${queryBodyStr} LIMIT ${perPage} OFFSET ${offset}`, [searchName]),
        ]).then(([countResult, result]) => {
            const total = +_.get(countResult, 'rows.0.count', 0);
            const data: IRestaurantInformationResponse[] = _.get(result, 'rows', []);
            const pagination: IPagination = {
                total,
                perPage: perPage,
                pageCount: Math.ceil((total) / perPage),
                currentPage: page,
            };

            return { data, pagination };
        });
    }


    async getRestaurantHasImagesByIds(restaurantUuids: string[]) {
        if (!restaurantUuids || restaurantUuids.length === 0) return [];
        const queryBodies = "FROM restaurant_has_image WHERE restaurant_uuid = ANY (?)";
        const res = await this.knex.raw(`SELECT * ${queryBodies}`, [restaurantUuids]);

        const rows: IRestaurantHasImagesResponse[] = res.rows;
        return rows
    }
}

