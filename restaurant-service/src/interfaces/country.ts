export interface Country {
    country_code: string;
    locales: string;
}