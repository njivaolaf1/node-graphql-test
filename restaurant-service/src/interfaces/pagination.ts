
export interface IPagination {
    total: string | number;
    pageCount: number;
    currentPage: number;
    perPage: number;
}

