
interface CustomCountry {
    code?: string;
    locales?: string[];
}

export interface IRestaurantInformation {
    restaurant_uuid: string;
    name: string;
    images: string[];
    country: CustomCountry;
}

export interface IRestaurantInformationResponse {
    restaurant_uuid: string;
    name: string;
    country_code?: string;
    country_locales?: string[];
}

export interface IRestaurantHasImagesResponse {
    restaurant_uuid: string;
    image_uuid: string;
}