
export interface IImage {
    imageUuid: string;
    url: string;
}

export interface IImagesResponse {
    images: IImage[]
}