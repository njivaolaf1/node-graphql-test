'use strict';

import * as _ from 'lodash';
import { IImage } from "../interfaces/image"
import { IRestaurantHasImagesResponse } from "../interfaces/restaurant"

function getAssociatedImagesIds(restaurantUuid: string, restaurantHasImages: IRestaurantHasImagesResponse[]): string[] {
    return _.chain(restaurantHasImages)
        .filter(a => a.restaurant_uuid === restaurantUuid)
        .map(a => a.image_uuid)
        .value()
}

function filterImagesByIds(imageIds: string[], images: IImage[]) {
    return _.filter(images, img => imageIds.includes(img.imageUuid))
}

export default {
    getAssociatedImagesIds,
    filterImagesByIds,
}