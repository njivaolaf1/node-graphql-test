'use strict';

import { ImageRepository } from "../repositories/image-repository";
import { RestaurantInfoRepository } from "../repositories/restaurant-information-repository";
import * as _ from 'lodash';
import { IRestaurantInformation, IRestaurantInformationResponse } from "../interfaces/restaurant";

import imageHelper from '../helpers/image.helper';

function buildRestaurant(item: IRestaurantInformationResponse, imageUrls: string[]): IRestaurantInformation {
    return ({
        restaurant_uuid: item.restaurant_uuid,
        name: item.name,
        images: imageUrls,
        country: {
            code: item.country_code,
            locales: item.country_locales
        }
    });
}

async function getRestaurantsInformations(_parent: any, args: any, context: any) {
    const restaurantInformationSource: RestaurantInfoRepository = context.dataSources.restaurantInformationSource;
    const imageSource: ImageRepository = context.dataSources.imageSource;
    const { perPage, currentPage, name, withImagesOnly } = args;

    const [{ data, pagination }, { images }] = await Promise.all(
        [
            restaurantInformationSource.getRestaurantsInformations({ perPage, currentPage, name, withImagesOnly }),
            imageSource.getImages(),
        ]
    );
    const restaurantHasImages = await restaurantInformationSource.getRestaurantHasImagesByIds(data.map(restauItem => restauItem.restaurant_uuid));

    const restaurants = _.chain(data)
        .map(item => {
            const associatedImagesIds = imageHelper.getAssociatedImagesIds(item.restaurant_uuid, restaurantHasImages);
            const associatedImageUrls = imageHelper.filterImagesByIds(associatedImagesIds, images).map(img => img.url);
            return buildRestaurant(item, associatedImageUrls);
        })
        .value()
    return ({ restaurants, pagination });
}

export default {
    build: () => ({ getRestaurantsInformations })
}