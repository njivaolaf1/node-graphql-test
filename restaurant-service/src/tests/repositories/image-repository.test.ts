import { ImageRepository } from "../../repositories/image-repository"

const mockGet = jest.fn();

jest.mock('apollo-datasource-rest', () => {
    class MockRESTDataSource { baseUrl = ''; get = mockGet; }
    class MockHTTPCache { }
    return {
        RESTDataSource: MockRESTDataSource,
        HTTPCache: MockHTTPCache,
    }
});

afterEach(() => {
    mockGet.mockReset();
});

describe('Instantiating', () => {
    it('should initialize successfully', () => {
        const imageRepositoryInstance = new ImageRepository({ baseUrl: "http://image-service:1234/" });
        expect(imageRepositoryInstance.baseURL).toEqual("http://image-service:1234/");
    });
})

describe('getImages()', () => {
    it('should call get successfully', async () => {

        mockGet.mockImplementation(() => ({
            "images": [{ "image_url": "some_url_examples.com" }]
        }));
        const imageRepositoryInstance = new ImageRepository({ baseUrl: "http://image-service:1234/" });
        const result = await imageRepositoryInstance.getImages();

        expect(mockGet).toHaveBeenCalledTimes(1);
        expect(mockGet).toHaveBeenCalledWith('images');

        expect(result).toEqual({
            "images": [{ "image_url": "some_url_examples.com" }]
        });

    })
})