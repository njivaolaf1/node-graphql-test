import { IPagination } from '../../interfaces/pagination';
import { IRestaurantInformationResponse, IRestaurantHasImagesResponse } from '../../interfaces/restaurant';
import { IImage } from '../../interfaces/image';
import RestaurantInformationResolver from '../../resolvers/restaurant-information-resolver'

const mockRestaurantInformationSource = {
    getRestaurantsInformations: jest.fn(),
    getRestaurantHasImagesByIds: jest.fn(),
};

const mockImageSource = {
    getImages: jest.fn(),
};
describe('building', () => {
    it('should build successfully', () => {
        const restaurantInfoResolverInstance = RestaurantInformationResolver.build();
        expect(restaurantInfoResolverInstance).toHaveProperty('getRestaurantsInformations');
    });
});

describe('getRestaurantsInformations()', () => {

    it('should call getRestaurantsInformations and return restaurants with pagination ', async () => {
        const data: IRestaurantInformationResponse[] = [
            {
                restaurant_uuid: 'rest1',
                name: 'Restau Nandows',
                country_code: "MRU",
                country_locales: ["LOC1"]
            },
            {
                restaurant_uuid: 'rest2',
                name: 'Super pizza',
                country_code: "FRA",
                country_locales: ["LOC2"]
            }
        ];

        const pagination: IPagination = {
            total: 100,
            perPage: 5,
            pageCount: 10,
            currentPage: 1,
        };

        const images: IImage[] = [
            {
                imageUuid: 'img_uu1',
                url: "http://someurl-test1.com"
            },
            {
                imageUuid: 'img_uu2',
                url: "http://someurl-test2.com"
            },
            {
                imageUuid: 'img_uu3',
                url: "http://someurl-test3.com"
            }
        ];

        const restaurantsHasImages: IRestaurantHasImagesResponse[] = [
            {
                restaurant_uuid: 'rest2',
                image_uuid: 'img_uu1'
            },
            {
                restaurant_uuid: 'rest2',
                image_uuid: 'img_uu1'
            }
        ];

        mockRestaurantInformationSource.getRestaurantsInformations.mockImplementationOnce(() => ({ data, pagination }))
        mockRestaurantInformationSource.getRestaurantHasImagesByIds.mockImplementationOnce(() => restaurantsHasImages)
        mockImageSource.getImages.mockImplementationOnce(() => ({ images }));

        const ds = {
            dataSources: {
                restaurantInformationSource: mockRestaurantInformationSource,
                imageSource: mockImageSource,
            }
        }

        const restaurantInfoResolverInstance = RestaurantInformationResolver.build();
        const reqParams = {
            perPage: 10,
            currentPage: 10,
            name: "zo",
            withImagesOnly: true
        }
        const result = await restaurantInfoResolverInstance.getRestaurantsInformations(null, reqParams, ds);
        expect(result).toEqual({
            "pagination":
            {
                "currentPage": 1,
                "pageCount": 10, "perPage": 5, "total": 100
            },
            "restaurants": [
                {
                    "country": {
                        "code": "MRU",
                        "locales": ["LOC1"]
                    },
                    "images": [],
                    "name": "Restau Nandows", "restaurant_uuid": "rest1"
                },
                {
                    "country": {
                        "code": "FRA",
                        "locales": ["LOC2"]
                    },
                    "images": ["http://someurl-test1.com"],
                    "name": "Super pizza", "restaurant_uuid": "rest2"
                }]
        });
    });
})