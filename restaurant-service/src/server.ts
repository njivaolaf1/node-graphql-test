import { ApolloServer } from 'apollo-server-express';
import express from 'express';
import config from 'config';
import RestaurantInformationResolver from './resolvers/restaurant-information-resolver';
import { loadSchemaSync } from '@graphql-tools/load';
import { GraphQLFileLoader } from '@graphql-tools/graphql-file-loader';
import { addResolversToSchema } from '@graphql-tools/schema';
import { join } from 'path';
import { RestaurantInfoRepository } from './repositories/restaurant-information-repository'
import { ImageRepository } from './repositories/image-repository'


const connectionConfig = {
  client: 'pg',
  connection: config.get('database'),
};

const imageServiceConfig = {
  baseUrl: config.get<any>('services').image.url
};


const schema = loadSchemaSync(join(__dirname, 'schema.graphql'), { loaders: [new GraphQLFileLoader()] });
const { getRestaurantsInformations } = RestaurantInformationResolver.build();


const resolvers = {
  Query: {
    restaurantsInformations: getRestaurantsInformations,
  },
};

const schemaWithResolvers = addResolversToSchema({
  schema,
  resolvers,
});

const main = async () => {
  const app = express();

  const server = new ApolloServer({
    schema: schemaWithResolvers,
    dataSources: () => ({
      restaurantInformationSource: new RestaurantInfoRepository(connectionConfig),
      imageSource: new ImageRepository(imageServiceConfig)
    })
  });


  await server.start();

  server.applyMiddleware({ app });

  app.listen({ port: config.get('server.port') }, () => console.info(
    `🚀 Server ready and listening at ==> http://localhost:${config.get('server.port')}${server.graphqlPath
    }`,
  ));
};

main().catch((error) => {
  console.error('Server failed to start', error);
});
