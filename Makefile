## Depending on the docker install, check if sudo is needed
sudoneeded := $(shell docker version >/dev/null 2>&1; echo $$?)
ifeq ($(sudoneeded), 1)
        DOCKERCMD := sudo docker
        DOCKERCOMPOSECMD := sudo docker-compose
else
        DOCKERCMD := docker
        DOCKERCOMPOSECMD := docker-compose
endif

setup:
	$(DOCKERCMD) network create dev

# launch containers
docker_up:
	$(DOCKERCOMPOSECMD) up -d

# stop containers
docker_down:
	$(DOCKERCOMPOSECMD) down

# build image
build:
	$(DOCKERCOMPOSECMD) up -d --build

# show logs
logs:
	$(DOCKERCOMPOSECMD) logs -ft

# see process container
ps:
	$(DOCKERCOMPOSECMD) ps